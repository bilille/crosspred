#! /usr/bin/env python

import pprint
import os
import argparse
from Bio import SeqIO

from lib.peptide import *
from lib.ptm import *

##### Define command line ############################################

desc = "Predict the PTM in a given peptid sequence"

command = argparse.ArgumentParser(prog = 'predict_PTM.py',
    description = desc,
    usage = '%(prog)s [options]')

command.add_argument('-s', '--sequence', nargs = '?',
    type = str,
    help = 'Peptid sequence (mutually exclusive with -f)')

command.add_argument('-f', '--fasta', nargs = '?',
    type = str,
    help = 'Input proteic fasta file (mutually exclusive with -s)')

command.add_argument('--ptm', nargs = '?',
    type = str,
    required = True,
    help = 'path of tabulated file with the allowed PTMs')

command.add_argument('-a', '--all',
    action = 'store_true',
    help = 'Display all possible PTMs')

command.add_argument('-n', '--nbPTMmax', nargs = '?',
    type = int,
    default = 5,
    help = 'Display all combinations of maximum --nbPTMmax PTMs (default 5)')


##### Read arguments of command line #################################
args = command.parse_args()

if not args.sequence and not args.fasta:
    raise Exception("You must give an input sequence either with --sequence or with --fasta")


##### Main ###########################################################
sequences = list()

if args.sequence:
    sequences.append(args.sequence)
else:
    if args.fasta and os.path.isfile(args.fasta):
        for record in SeqIO.parse(args.fasta, "fasta"):
            sequences.append(record.seq)
    else:
        raise Exception(str(args.fasta) + " is not found")

ptm_predictor = PTM(args.ptm, args.nbPTMmax)

for sequence in sequences:
    peptide = Peptide(sequence)
    if args.all:
        all_positions = ptm_predictor.predict_modifiable_positions(peptide.sequence)
        nb_pos = len(all_positions.keys())
        nb_ptm = 0
        for pos in all_positions:
            for modif in all_positions[pos]:
                nb_ptm += 1
        print('All possible PTMs: {} possible PTMs on {} different positions.'.format(nb_ptm, nb_pos))
    else:
        peptide.ptm = ptm_predictor.predictPTM(peptide.sequence)
        nb_combi = len(peptide.ptm)
        print('All combinations of maximum {} PTMs: {} possible combinations.'.format(args.nbPTMmax, nb_combi))

    #~ print('Display all possible PTMs')
    #~ all_positions = ptm_predictor.predict_modifiable_positions(peptide.sequence)
    #~ pprint.pprint(all_positions)
    #~ print('Display all combinations of maximum --nbPTMmax PTMs (default 5)')
    #~ peptide.ptm = ptm_predictor.predictPTM(peptide.sequence)
    #~ pprint.pprint(peptide.ptm)

