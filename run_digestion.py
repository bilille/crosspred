#! /usr/bin/env python

import os
import pprint
import argparse
from Bio import SeqIO

from lib.digestor import *
from lib.ptm import *


##### Define command line ############################################

desc = "Enzymatic digestion"

command = argparse.ArgumentParser(prog = 'run_digestion.py',
    description = desc,
    usage = '%(prog)s [options]')

command.add_argument('-f', '--fasta', nargs = '?',
    type = str,
    required = True,
    help = 'One input proteic fasta file (required)')

command.add_argument('-o', '--output', nargs = '?',
    type = str,
    default = 'output.fasta',
    help = 'Output file name')

command.add_argument('-m', '--misscleavages', nargs = '?',
    type = int,
    default = 1,
    help = 'Number of miss-cleavages allowed ([0-3] default 1)')

command.add_argument('--peptidase', nargs = '?',
    type = str,
    required = True,
    help = "path of tabulated config file of possible peptidases (required)")

command.add_argument('--crosslinks', nargs = '?',
    type = str,
    help = "path of tabulated config file of possible crosslinks (optional)")

command.add_argument('--ptm', nargs = '?',
    type = str,
    help = "path of tabulated file with the allowed PTMs (optional)")

command.add_argument('-n', '--nbPTMmax', nargs = '?',
    type = int,
    default = 5,
    help = 'Max number of PTMs in a peptide (default 5)')


##### Read arguments of command line #################################
args = command.parse_args()

if args.misscleavages <0 or args.misscleavages >3:
    raise ValueError("The number of allowed miss-cleavages must comprised be between 0 and 3.")


##### Main ###########################################################

## Digestion

d = Digestor(args.peptidase)
d.misscleavage = args.misscleavages

# Create a dict of peptides from all input fasta sequences
peptides_dict_list = list()
for record in SeqIO.parse(args.fasta, "fasta"):
    # Get a dict of digested peptides
    peptides_dict_list.append(d.digest(record))

# Merge all lists into one
peptides_dict = peptides_dict_list[0].copy()
for pep_dict in peptides_dict_list[1:]:
    for pep in pep_dict:
        if pep not in peptides_dict:
            peptides_dict[pep] = pep_dict[pep]
        else:
            peptides_dict[pep] = peptides_dict[pep].union(pep_dict[pep])

nb_pep_in_merged_list = len(peptides_dict)


## Filtering

# Filter out peptids with less than 4 AA
peptides_dict = {elt:value for elt, value in peptides_dict.items() if len(elt) >= 4}

# If a crosslinks file is given, filter out the non-linkable peptids
if args.crosslinks and os.path.isfile(args.crosslinks):
    crosslinks_predictor = Predictor(args.crosslinks)
    peptides_dict = {elt:value for elt, value in peptides_dict.items() if is_peptide_linkable(crosslinks_predictor, elt)}

# If a PTMs file is given, look for the PTMs.
# The peptides will now be stored as Peptide objects in a list.
if args.ptm and os.path.isfile(args.ptm):
    ptm_predictor = PTM(args.ptm, args.nbPTMmax)

peptide_obj_list = list()
for pep in peptides_dict:
    pep_obj = Peptide(pep, peptides_dict[pep])
    try:
        pep_obj.ptm = ptm_predictor.predictPTM(pep)
    except:
        pep_obj.ptm = []
    peptide_obj_list.append(pep_obj)

# Print results
Digestor.export_as_fasta(peptide_obj_list, args.output)

