#!/bin/sh

### Initialisation
SCRIPT=$(readlink -f $0)
BASEDIR=$(dirname $SCRIPT)
ROOTDIR=$BASEDIR/..
PEPTIDASE=$BASEDIR/peptidases.tsv
CROSSLINKS=$BASEDIR/crosslinks.tsv
PTM=$BASEDIR/PTMs.tsv
NB_PTM_MAX=2

### Importing TAP library
if [ -z "$LIBTAP_SH_HOME" ]
then
    if [ ! -f $BASEDIR/libtap.sh ]
    then
       wget --quiet "http://git.eyrie.org/?p=devel/c-tap-harness.git;a=blob_plain;f=tests/tap/libtap.sh;hb=HEAD" -O $BASEDIR/libtap.sh
    fi
    LIBTAP="$BASEDIR/libtap.sh"
else
    LIBTAP="$LIBTAP_SH_HOME/libtap.sh"
fi
. "$LIBTAP"



### Testing
plan 4

if [ ! -d "$BASEDIR/output/" ]; then
    mkdir "$BASEDIR/output/"
fi


## Trypsin
gawk -i inplace -F '\t' '{ if ($1=="Name") print; else print $1"\t"$2"\t"$3"\t"$4"\tno"; }' $PEPTIDASE
gawk -i inplace  -F '\t' '{ if ($1=="trypsin") print $1"\t"$2"\t"$3"\t"$4"\tyes"; else print; }' $PEPTIDASE


# with 0 misscleavage
rm -rf $BASEDIR/output/lysozyme_human_trypsin_m0*
python $ROOTDIR/crosspred.py \
    -f $BASEDIR/lysozyme_human.fa \
    --peptidase $PEPTIDASE \
    --crosslinks $CROSSLINKS \
    --ptm $PTM \
    --nbPTMmax $NB_PTM_MAX \
    -m 0 \
    -o $BASEDIR/output/lysozyme_human_trypsin_m0.tsv
sort $BASEDIR/output/lysozyme_human_trypsin_m0.tsv > $BASEDIR/output/lysozyme_human_trypsin_m0_sorted.tsv

diff $BASEDIR/output/lysozyme_human_trypsin_m0_sorted.tsv $BASEDIR/expected/lysozyme_human_trypsin_m0_sorted.tsv > $BASEDIR/diff_trypsin_m0.txt
DIFF=$(cat $BASEDIR/diff_trypsin_m0.txt | wc -l)
if [ ! $DIFF -eq 0 ]
then
    echo "\n" $DIFF" different lines between output file and expected file."
else
    rm $BASEDIR/diff_trypsin_m0.txt
fi
ok 'Lysozyme with trypsin and 0 misscleavage' [ $DIFF -eq 0 ]


# with 1 misscleavage
rm -rf $BASEDIR/output/lysozyme_human_trypsin_m1*
python $ROOTDIR/crosspred.py \
    -f $BASEDIR/lysozyme_human.fa \
    --peptidase $PEPTIDASE \
    --crosslinks $CROSSLINKS \
    --ptm $PTM \
    --nbPTMmax $NB_PTM_MAX \
    -m 1 \
    -o $BASEDIR/output/lysozyme_human_trypsin_m1.tsv
sort $BASEDIR/output/lysozyme_human_trypsin_m1.tsv > $BASEDIR/output/lysozyme_human_trypsin_m1_sorted.tsv

diff $BASEDIR/output/lysozyme_human_trypsin_m1_sorted.tsv $BASEDIR/expected/lysozyme_human_trypsin_m1_sorted.tsv > $BASEDIR/diff_trypsin_m1.txt
DIFF=$(cat $BASEDIR/diff_trypsin_m1.txt | wc -l)
if [ ! $DIFF -eq 0 ]
then
    echo "\n"$DIFF" different lines between output file and expected file."
else
    rm $BASEDIR/diff_trypsin_m1.txt
fi
ok 'Lysozyme with trypsin and 1 misscleavage' [ $DIFF -eq 0 ]


## Arg-C
gawk -i inplace -F '\t' '{ if ($1=="Name") print; else print $1"\t"$2"\t"$3"\t"$4"\tno"; }' $PEPTIDASE
gawk -i inplace  -F '\t' '{ if ($1=="Arg-C") print $1"\t"$2"\t"$3"\t"$4"\tyes"; else print; }' $PEPTIDASE


# with 0 misscleavage
rm -rf $BASEDIR/output/lysozyme_human_argc_m0*
python $ROOTDIR/crosspred.py \
    -f $BASEDIR/lysozyme_human.fa \
    --peptidase $PEPTIDASE \
    --crosslinks $CROSSLINKS \
    --ptm $PTM \
    --nbPTMmax $NB_PTM_MAX \
    -m 0 \
    -o $BASEDIR/output/lysozyme_human_argc_m0.tsv
sort $BASEDIR/output/lysozyme_human_argc_m0.tsv > $BASEDIR/output/lysozyme_human_argc_m0_sorted.tsv

diff $BASEDIR/output/lysozyme_human_argc_m0_sorted.tsv $BASEDIR/expected/lysozyme_human_argc_m0_sorted.tsv > $BASEDIR/diff_argc_m0.txt
DIFF=$(cat $BASEDIR/diff_argc_m0.txt | wc -l)
if [ ! $DIFF -eq 0 ]
then
    echo "\n"$DIFF" different lines between output file and expected file."
else
    rm $BASEDIR/diff_argc_m0.txt
fi
ok 'Lysozyme with Arg-C and 0 misscleavage' [ $DIFF -eq 0 ]


# with 1 misscleavage
rm -rf $BASEDIR/output/lysozyme_human_argc_m1*
python $ROOTDIR/crosspred.py \
    -f $BASEDIR/lysozyme_human.fa \
    --peptidase $PEPTIDASE \
    --crosslinks $CROSSLINKS \
    --ptm $PTM \
    --nbPTMmax $NB_PTM_MAX \
    -m 1 \
    -o $BASEDIR/output/lysozyme_human_argc_m1.tsv
sort $BASEDIR/output/lysozyme_human_argc_m1.tsv > $BASEDIR/output/lysozyme_human_argc_m1_sorted.tsv

diff $BASEDIR/output/lysozyme_human_argc_m1_sorted.tsv $BASEDIR/expected/lysozyme_human_argc_m1_sorted.tsv > $BASEDIR/diff_argc_m1.txt
DIFF=$(cat $BASEDIR/diff_argc_m1.txt | wc -l)
if [ ! $DIFF -eq 0 ]
then
    echo "\n"$DIFF" different lines between output file and expected file."
else
    rm $BASEDIR/diff_argc_m1.txt
fi
ok 'Lysozyme with Arg-C and 1 misscleavage' [ $DIFF -eq 0 ]



