#! /usr/bin/env python3


"""
Author: Isabelle GUIGON
Date: 2019-07-25
Developed on Python 3.5.2

This script takes as input the directory of theoretical masses
created during the first step (process_crosspred_output_masses.py)
and a file of experimental masses.

The file with experimental masses can:
- either have only a mass on each line
- or be a tabulated file with several columns (no matter the column order). 
In that case the script will look for a column name containing "Precursor m/z" or "Precursor mass".
If a column containing "Precursor charge" in its name is present,
the script will output only results where the theoretical and experimental charges are the same.
"""

import re
import os
import sys
import json
import argparse
import pprint
import math


def are_almost_equal(m_exp, m_theo, ppm):
    return (abs(1000000*(m_exp-m_theo)/m_theo) < ppm)


def main(args):
    output_fh = open(args.output, 'w')
    header = "Exp_mass\t"
    header += "Theo_mass\t"
    header += "Charge\t"
    header += "Peptide1\t"
    header += "Peptide1_pos\t"
    header += "Peptide1_PTMs_names\t"
    header += "Peptide1_PTMs_pos\t"
    header += "Peptide2\t"
    header += "Peptide2_pos\t"
    header += "Peptide2_PTMs_names\t"
    header += "Peptide2_PTMs_pos\t"
    header += "Crosslink\t"
    header += "Position\t"
    header += "Raw_formula"

    output_fh.write(header + "\n")


    # Read the experimental masses and store them
    # Either the file has only one column with the experimental masses
    # or it contains several columns and we look for the columns with
    # mass ("Precursor m/z" or "Precursor mass") and charge ("Precursor charge")
    # (the columns names must contain what's above, case is ignored)
    exp_masses = dict()
    with open(args.exp, 'r') as exp_fh:
        header = exp_fh.readline().strip()
        fields = header.split("\t")
        col_charge = 0
        col_mass = 0
        for i, col in enumerate(fields):
            col = col.strip("'\"")
            if re.search(r"Precursor m/z", col, re.IGNORECASE) or re.search(r"Precursor mass", col, re.IGNORECASE):
                col_mass = i
            if re.search(r"Precursor charge", col, re.IGNORECASE):
                col_charge = i
        for line in iter(exp_fh.readline, ''):
            fields = line.split("\t")
            if line.strip() != '':
                try:
                    exp_masses[float(fields[col_mass].strip("'\""))] = int(fields[col_charge].strip("'\""))
                except:
                    exp_masses[float(fields[col_mass].strip("'\""))] = -1

    for exp_mass in sorted(exp_masses.keys()):
        exp_mass_trunc = math.trunc(exp_mass)
        theo_files = list()
        theo_files.append(os.path.join(args.theodir, "theoric_mass_{}.tsv".format(exp_mass_trunc)))
        if math.trunc(exp_mass - args.precision) == exp_mass_trunc - 1:
            theo_files.append(os.path.join(args.theodir, "theoric_mass_{}.tsv".format(exp_mass_trunc-1)))
        if math.trunc(exp_mass + args.precision) == exp_mass_trunc + 1:
            theo_files.append(os.path.join(args.theodir, "theoric_mass_{}.tsv".format(exp_mass_trunc+1)))
        for theo_file in theo_files:
            if not os.path.isfile(theo_file):
                continue
            with open(theo_file) as theo_fh:
                for line in theo_fh.readlines():
                    line = line.strip()
                    fields = line.split("\t")
                    theo_mass = float(fields[0])
                    theo_charge = int(fields[1])
                    # if the experimental charge is given we check that it is the same
                    # than the theoric charge
                    if exp_masses[exp_mass] != -1 and theo_charge != exp_masses[exp_mass]:
                        continue
                    if theo_mass < exp_mass - args.precision:
                        continue
                    if theo_mass > exp_mass + args.precision:
                        break
                    if are_almost_equal(exp_mass, theo_mass, args.precision):
                        output_fields = list()
                        output_fields.append(exp_mass)
                        output_fields += fields
                        output_fields = [str(x) for x in output_fields]
                        output_fh.write("\t".join(output_fields) + "\n")

    output_fh.close()


if __name__ == '__main__':

    desc = "Script to compare experimental and theoric masses. "
    desc += "The experimental file must either contain only one column with masses, "
    desc += "or it must have a column containing 'Precursor m/z' or 'Precursor mass' in the column name. "
    desc += "If a column containing 'Precursor charge' is present, the script will also check "
    desc += "that theoretical and experimental masses have the same charge."

    command = argparse.ArgumentParser(prog = 'compare_masses.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-e', '--exp', nargs = '?',
        type = str,
        required = True,
        help = 'A file with experimental masses (one per line) (required)')

    command.add_argument('-t', '--theodir', nargs = '?',
        type = str,
        required = True,
        help = 'Path of directory where the theoric masses are stored (required)')

    command.add_argument('-o', '--output', nargs = '?',
        type = str,
        default = 'output.tsv',
        help = 'Output file name')

    command.add_argument('-p', '--precision', nargs = '?',
        type = float,
        default = 5,
        help = 'Precision for comparing masses given in ppm (default 5 ppm)')

    args = command.parse_args()

    main(args)
