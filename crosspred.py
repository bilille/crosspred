#! /usr/bin/env python3


"""
Author: Isabelle GUIGON
Date: 2018-09-14
Developed on Python 3.5.2
"""

import os
import sys
import argparse
import pprint
import itertools
import tempfile
from datetime import datetime
from Bio import SeqIO
from multiprocessing import Pool
from functools import partial

from lib.digestor import Digestor
from lib.peptide import *
from lib.molmass import *
from lib.elements import *
from lib.crosslink import *
from lib.utils import *
from lib.ptm import *

#~ import tracemalloc

#~ tracemalloc.start()


def main(args):
    startTime = datetime.now()

    if not args.cpu:
        args.cpu = os.cpu_count()-1

    # don't allocate more CPUs than what we really have
    if args.cpu > os.cpu_count():
        args.cpu = os.cpu_count()-1

    p = Pool(args.cpu)

    ### Write output header
    output_file = os.path.join( args.outdir, 'output.tsv' )
    output_fh = open(output_file, 'w')
    header = "Peptide1\t"
    header += "Peptide1_PTMs_names\t"
    header += "Peptide1_PTMs_pos\t"
    header += "Peptide2\t"
    header += "Peptide2_PTMs_names\t"
    header += "Peptide2_PTMs_pos\t"
    header += "Crosslink\t"
    header += "Position\t"
    header += "Raw_formula\t"
    header += "Mass\t"
    header += "n=1\t"
    header += "n=2\t"
    header += "n=3\t"
    header += "n=4\n"
    output_fh.write(header)
    output_fh.close()


    ### Initialise a Predictor object with the given possible crosslinks
    crosslinks_predictor = Predictor(args.crosslinks)

    ### Initialise a PTM object with the given possible PTMs
    ### and the max nb of PTMs allowed in a sequence
    ptm_predictor = PTM(args.ptm, args.nbPTMmax)

    ### Digestion and filtration of digested peptides

    d = Digestor(args.peptidase)
    d.misscleavage = args.misscleavages

    # Digest the input sequences in parallel
    peptides_dict_list = p.map(d.digest, SeqIO.parse(args.fasta, "fasta"))

    endOfDigestionTime = datetime.now()

    # Merge all lists into one
    peptides_dict = peptides_dict_list[0].copy()
    pep_number = list()
    pep_number.append(len(peptides_dict))

    for pep_dict in peptides_dict_list[1:]:
        pep_number.append(len(pep_dict))
        for pep in pep_dict:
            if pep not in peptides_dict:
                peptides_dict[pep] = pep_dict[pep]
            else:
                peptides_dict[pep] = peptides_dict[pep].union(pep_dict[pep])

    nb_pep_in_merged_list = len(peptides_dict)

    endOfMergingTime = datetime.now()

    # Filter out the peptides with length < 4 AA and the peptides that
    # don't have any of the AA that can be involved in a crosslink
    peptides_dict = {elt:value for elt, value in peptides_dict.items() if len(elt) >= 4 and is_peptide_linkable(crosslinks_predictor, elt)}

    endOfFiltrationTime = datetime.now()

    # Convert to Peptide objects and predict the PTMs
    peptide_obj_list = list()
    for pep in peptides_dict:
        pep_obj = Peptide(pep, peptides_dict[pep])
        try:
            pep_obj.ptm = ptm_predictor.predictPTM(pep)
        except:
            pep_obj.ptm = []
        peptide_obj_list.append(pep_obj)

    peptides_dict = dict()
    nb_pep_in_filtrated_list = len(peptide_obj_list)

    # Export the filtered digested peptides to fasta and TSV
    peptides_file_fasta = os.path.join( args.outdir, 'digested_peptides.fasta' )
    Digestor.export_as_fasta(peptide_obj_list, peptides_file_fasta)
    peptides_file_tsv = os.path.join( args.outdir, 'digested_peptides.tsv' )
    Digestor.export_as_tsv(peptide_obj_list, peptides_file_tsv)

    endOfPTMpredictionTime = datetime.now()

    ### Combinations and crosslinks

    # Get all combinations of 2 peptides
    nb_combinaisons = nb_pep_in_filtrated_list * (nb_pep_in_filtrated_list - 1) / 2
    nb_combinaisons = int(nb_combinaisons)

    # don't allocate more CPU than the number of combinations
    # (elsewhere size_chunks will be 0 and itertools.islice will return empty chunks)
    if args.cpu > nb_combinaisons:
        args.cpu = nb_combinaisons
    args.cpu = int(args.cpu)

    size_chunks = int(nb_combinaisons / args.cpu)

    peptide_obj_list.sort(key = lambda x:x.sequence)
    combination_array = itertools.combinations(peptide_obj_list, 2)

    # Split the array of combinations into chunks
    tempfile_list = list()
    my_chunks = list()
    for i in range(args.cpu-1):
        chunk = itertools.islice( combination_array, 0, size_chunks )
        # 'chunk' is an iterable containing 'size_chunks' couples of peptids
        my_chunks.append(list(chunk))
    chunk = itertools.islice( combination_array, 0, None )
    my_chunks.append(list(chunk))

    # Calculate the crosslinks (in parallel)
    get_crosslinks_with_pep_dict = partial(
                                    get_all_crosslinks_for_tuple_peptides,
                                    crosslinks_predictor=crosslinks_predictor,
                                    ptm_predictor=ptm_predictor,
                                    tmp=args.tmp
                                    ) # create a new function encapsulating
                                      # function 'get_all_crosslinks_for_tuple_peptides'
                                      # and attaching arguments
                                      # 'crosslinks_predictor' and 'ptm_predictor'
                                      # (we can give only 1 arg to imap)

    tempfile_list.extend(p.imap( get_crosslinks_with_pep_dict, my_chunks ))

    tempfile_list = list(tempfile_list)

    # Concatenate all tmp files into output file
    cat_cmd = "cat "
    cat_cmd += " ".join(tempfile_list)
    cat_cmd += ">> "
    cat_cmd += output_file
    os.system(cat_cmd)

    # Count tmp files and remove them
    nb_tmp_files = 0
    for tmpfile in tempfile_list:
        nb_tmp_files +=1
        os.remove(tmpfile)

    endOfCombinationTime = datetime.now()


    #~ ### Print informations about the nb of peptides
    #~ sys.stderr.write("-----------------------------\n")
    #~ for i, elt in enumerate(pep_number):
        #~ sys.stderr.write("{} peptides for {}-th list\n".format(elt, i))
    #~ sys.stderr.write("{} peptides in merged list\n".format(nb_pep_in_merged_list))
    #~ sys.stderr.write("{} peptides after filtration\n".format(nb_pep_in_filtrated_list))
    #~ sys.stderr.write("{} combinaisons\n".format(nb_combinaisons))
    #~ sys.stderr.write("{} chunks size\n".format(size_chunks))
    #~ sys.stderr.write("{} temporary files\n".format(nb_tmp_files))

    #~ ### Print informations about sub-part durations
    #~ sys.stderr.write("-----------------------------\n")
    #~ sys.stderr.write("Digestion time: {}.\n".format(endOfDigestionTime - startTime))
    #~ sys.stderr.write("Merging time: {}.\n".format(endOfMergingTime - endOfDigestionTime))
    #~ sys.stderr.write("Filtration time: {}.\n".format(endOfFiltrationTime - endOfMergingTime))
    #~ sys.stderr.write("PTMs prediction time: {}.\n".format(endOfPTMpredictionTime - endOfFiltrationTime))
    #~ sys.stderr.write("Combination time: {}.\n".format(endOfCombinationTime - endOfPTMpredictionTime))


    #~ snapshot = tracemalloc.take_snapshot()
    #~ top_stats = snapshot.statistics('lineno')

    #~ print("[ Top 10 ]")
    #~ for stat in top_stats[:10]:
        #~ print(stat)

if __name__ == '__main__':

    desc = "CROSSPRED pipeline"

    command = argparse.ArgumentParser(prog = 'crosspred.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-f', '--fasta', nargs = '?',
        type = str,
        required = True,
        help = 'One input proteic fasta file (required)')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        required = True,
        help = 'Output directory (required)')

    command.add_argument('-c', '--cpu', nargs = '?',
        type = int,
        help = 'Number of CPU to use (default: nb of CPU of the machine, minus 1)')

    command.add_argument('-m', '--misscleavages', nargs = '?',
        type = int,
        default = 1,
        help = 'Number of miss-cleavages allowed ([0-3] default: 1)')

    command.add_argument('--peptidase', nargs = '?',
        type = str,
        required = True,
        help = "path of tabulated config file of possible peptidases (required)")

    command.add_argument('--crosslinks', nargs = '?',
        type = str,
        required = True,
        help = "path of tabulated config file of possible crosslinks (required)")

    command.add_argument('--ptm', nargs = '?',
        type = str,
        help = "path of tabulated file with the allowed PTMs (optional)")

    command.add_argument('-n', '--nbPTMmax', nargs = '?',
        type = int,
        default = 5,
        help = 'Max number of occasional PTMs in a peptide (default: 5)')

    command.add_argument('-t', '--tmp', nargs = '?',
        type = str,
        help = 'Path to tmp dir (default: your TMP environment variable)')

    args = command.parse_args()

    if args.misscleavages <0 or args.misscleavages >3:
        raise ValueError("The number of allowed miss-cleavages must comprised be between 0 and 3.")

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    main(args)

