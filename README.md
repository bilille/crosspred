# INSTALLATION

All scripts work with Python 3.  
Dependency: biopython.


## Manual install

Since there is few dependencies you can manually install Python3 and then biopython.

```bash
sudo apt-get update
sudo apt-get install python3
sudo apt-get install python3-pip
sudo pip3 install biopython
```


## Install with conda

Alternatively you can install conda and use a conda environment.

Install conda:
```bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash ~/Miniconda3-latest-Linux-x86_64.sh -b -p $HOME/miniconda3
export PATH="$HOME/miniconda3/bin:$PATH"
source ~/.bashrc
```

Create and activate a conda environment:
```bash
conda env create -f conda_crosspredEnv.yml
conda activate crosspredEnv
```


# USAGE

## CROSSPRED pipeline

CROSSPRED is a tool to predict crosslinks between peptides.  

First it will digest the input proteins into peptides, using the peptidase specified in file given with option --peptidase (default: trypsin).  
The digested peptides may contain up to -m misscleaved sites (default: 1 misscleavage).  

Then it will simulate all possible crosslinks between the peptides with at least 4 aminoacids, following the rules specified in file given with option --crosslinks.  

If a file is specified with option --ptm, all possible combinations of PTMs (Post-Translational Modifications) in each peptide will be generated.

Output file is a tabulated file containing information about the 2 crosslinked peptides, their optional PTMs, the crosslink position and aminoacids involved,
the raw formula and the crosslinked complex masses with 0 to 4 charges.

/!\ Be aware that because all combinations of misscleavages and PTMs are generated, the output file and the required disk space may be very important.


### Options

```bash
./crosspred.py -h
```

```
usage: crosspred.py [options]

CROSSPRED pipeline

optional arguments:
  -h, --help            show this help message and exit
  -f [FASTA], --fasta [FASTA]
                        One input proteic fasta file (required)
  -o [OUTPUT], --output [OUTPUT]
                        Output file name (default: output.tsv)
  -c [CPU], --cpu [CPU]
                        Number of CPU to use (default: nb of CPU of the
                        machine, minus 1)
  -m [MISSCLEAVAGES], --misscleavages [MISSCLEAVAGES]
                        Number of miss-cleavages allowed ([0-3] default: 1)
  --peptidase [PEPTIDASE]
                        path of tabulated config file of possible peptidases
                        (required)
  --crosslinks [CROSSLINKS]
                        path of tabulated config file of possible crosslinks
                        (required)
  --ptm [PTM]           path of tabulated file with the allowed PTMs
                        (optional)
  -n [NBPTMMAX], --nbPTMmax [NBPTMMAX]
                        Max number of occasional PTMs in a peptide (default:
                        5)
  -t [TMP], --tmp [TMP]
                        Path to tmp dir (default: your TMP environment
                        variable)
```

### Input files

The **--peptidase** file (required) is a tabulated file with a list of possible peptidases.  
You can add new peptidases provided that you respect the file format.  
**Only one peptidase can be chosen for the analysis** (column "Activated").

Example of **peptidase** file:
```
Name	Cleave	Not_cleave	N/C_term	Activated
trypsin	K,R	P	C	yes
Arg-C	R	P	C	no
Asp-N	DD,ND		N	no
CNBr	M		C	no
GluC	DE	P	C	no
```

The **crosslinks** file (required) is a tabulated file with the list of crosslinks that will be searched for during the analysis.

Example of **crosslinks** file:
```
AA1	AA2	Molecule_lost	Activated
C	C	-H2	yes
C	C	-H2O	yes
D	K	-H2O	yes
E	K	-H2O	yes
K	D	-H2O	yes
K	E	-H2O	yes
W	W	-H2	yes
Y	Y	-H2	yes
```

The **ptm** (optional) file is a tabulated file with a list of PTMs that can affect the amino acids.
The PTM (Post-Translational Modification) can either affect all involved amino acids (frequency: always)
or only affect some of the involved amino acids randomly (frequency: occasional).
In this last case the maximum number of amino acids than can be modified by an occasional PTM
in a peptid is set by option **--nbPTMmax**.

Example of **ptm** file:
```
Name	Monoisitopic_mass_change	Average_mass_change	AA	Frequency	Activated
C	57.021464	57.021464	C	always	yes
M	14.01565	14.0269	K,R	occasional	yes
O	15.99491	15.9994	M,P	occasional	yes
A	42.01056	42.0373	K	occasional	yes
P	79.96633	79.9799	S,T	occasional	yes
D	0.98402	0.9847	N,Q	occasional	yes
```

Signification of one-letter shortcuts:

C: carbamidomethylation  
M: methylation  
O: oxidation  
A: acetylation  
P: phosphorylation  
D: deamidation  


### Output file

The output file is a tabulated file with the following columns:  
- Peptide1:	peptide 1 sequence  
- Peptide1_pos:	peptide 1 position(s) in the input proteins  
- Peptide1_PTMs_names:	name(s) of PTMs in peptide 1  
- Peptide1_PTMs_pos:	position(s) of PTMs in peptide 1  
- Peptide2:	peptide 2 sequence  
- Peptide2_pos:	peptide 2 position(s) in the input proteins  
- Peptide2_PTMs_names:	name(s) of PTMs in peptide 2  
- Peptide2_PTMs_pos:	position(s) of PTMs in peptide 2  
- Crosslink:	AA involved in the crosslink  
- Position:	positions of AA involved in the crosslink (peptide1-peptide2)  
- Raw_formula:	raw formula of the crosslinked complex  
- Mass:	mass of the crosslinked complex  
- n=1:	mass of the crosslinked complex with n=1  
- n=2:	mass of the crosslinked complex with n=2  
- n=3:	mass of the crosslinked complex with n=3  
- n=4:	mass of the crosslinked complex with n=4  


## Comparison of theoretical and experimental masses

This analysis takes place in two steps:

### Theoretical masses processing

During this step, the theoretical masses created with the CROSSPRED pipeline are sorted and written in several files named after the whole part of the mass.
For instance the file *theoric_mass_832.tsv* will contain all masses between 832 and 833 (excluded) as well as the informations of the corresponding crosslinks.

The script takes as input a file produced by CROSSPRED pipeline and the path and name of output directory which will contain the theoretical masses.

This step allows us to save time and computing resources on the comparison step.
Plus, the user does not need to redo this step if he/she wants to compare the same theoretical masses to another experimental masses file.


```bash
./process_crosspred_output_masses.py -h
```

```
usage: process_crosspred_output_masses.py [options]

Script to process the theoric masses produced by CROSSPRED pipeline.

optional arguments:
  -h, --help            show this help message and exit
  -t [THEO], --theo [THEO]
                        An output tabulated file produced by CROSSPRED
                        pipeline (required)
  -o [OUTDIR], --outdir [OUTDIR]
                        Output directory that will store the theoric masses
```


### Comparison of masses

The second step is the comparison of masses literaly speaking.  
It takes as input the directory of theoretical masses created during the previous step and a file of experimental masses.

The file with experimental masses can:
- either have only a mass on each line
- or be a tabulated file with several columns (no matter the column order). In that case the script will look for a column name containing "Precursor m/z" or "Precursor mass".
If a column containing "Precursor charge" in its name is present, the script will output only results where the theoretical and experimental charges are the same.

The theoretical and experimental masses must correspond with a certain precision (given by the user, default 5 ppm), according to the following calculation:
```
1,000,000 x (exp_mass - theo_mass) / theo_mass <= precision
```


```bash
./compare_masses.py -h
```

```
usage: compare_masses.py [options]

Script to compare experimental and theoric masses

optional arguments:
  -h, --help            show this help message and exit
  -e [EXP], --exp [EXP]
                        A file with experimental masses (one per line)
                        (required)
  -t [THEODIR], --theodir [THEODIR]
                        Path of directory where the theoric masses are stored
                        (required)
  -o [OUTPUT], --output [OUTPUT]
                        Output file name
  -p [PRECISION], --precision [PRECISION]
                        Precision for comparing masses given in ppm (default 5
                        ppm)
```

### Output file

The output file is a tabulated file with the following columns:

- Exp_mass
- Theo_mass
- Charge
- Peptide1
- Peptide1_pos
- Peptide1_PTMs_names
- Peptide1_PTMs_pos
- Peptide2
- Peptide2_pos
- Peptide2_PTMs_names
- Peptide2_PTMs_pos
- Crosslink
- Position
- Raw_formula

Columns "Peptide1" to "Raw_formula" are taken from CROSSPRED file of theoretical masses.
The matching experimental mass as well as the theoretical mass and the theoretical charge are outputed in the three first columns.
