"""Class to determine all possible PTMs that can be present in a peptid
given its raw formula"""

import os
import sys
import itertools

class PTM():

    def __init__(self, ptm_file, nb_PTMs_max=5):
        """Initialise class with a tabulated file
        containing the following columns:
        'Name', 'Monoisitopic_mass_change', 'Average_mass_change', 'AA', 'Activated'
        'Name' is the name of the PTM (for instance 'methylation')
        'AA' contains the list of AA that can undergo the modification,
            separated by commas
        'Activated' must contain 'yes' or 'no'. If 'no', we won't look for that type of PTM.
        self.authorised_ptms is a dict with key=AA and
            value=dict with 2 keys: 'always' and 'occasional' containing
            each a list of PTMs names involving the corresponding AA.
            For instance:
            {'C': {'always': ['carbamidomethylation'], 'occasional': []},
             'K': {'always': [], 'occasional': ['methylation', 'acetylation']},
             'M': {'always': [], 'occasional': ['oxidation']},
             'T': {'always': [], 'occasional': ['phosphorylation']}
            }
        self.mass_ptms is a dict with key=PTM name and value=mass change.
        For instance:
        {'deamidation': '0.98402',
         'acetylation': '42.01056',
         'carbamidomethylation': '57.021464',
         'phosphorylation': '79.96633',
         'oxidation': '15.99491',
         'methylation': '14.01565'
        }
        """
        self.nb_PTMs_max = nb_PTMs_max
        self.authorised_ptms = dict()
        self.mass_ptms = dict()
        if ptm_file and os.path.isfile(ptm_file):
            ptm_fh = open(ptm_file, 'r')
            header = ptm_fh.readline()
            if header.strip() != "Name\tMonoisitopic_mass_change\tAverage_mass_change\tAA\tFrequency\tActivated":
                raise Exception("PTMs file does not have the good format. Columns must be 'Name', 'Monoisitopic_mass_change', 'Average_mass_change', 'AA', 'Frequency' and 'Activated'")
            for line in iter(ptm_fh.readline, ''):
                fields = line.rstrip().split("\t")
                if len(fields) == 6:
                    if fields[0] == 'Name':
                        continue
                    if fields[5] == 'no':
                        continue
                    list_AA = fields[3].split(',')
                    for AA in list_AA:
                        if AA.strip():
                            if AA.strip() not in self.authorised_ptms:
                               self.authorised_ptms[AA.strip()] = dict()
                               self.authorised_ptms[AA.strip()]['always'] = list()
                               self.authorised_ptms[AA.strip()]['occasional'] = list()
                            self.authorised_ptms[AA.strip()][fields[4]].append(fields[0])
                            self.mass_ptms[fields[0]] = float(fields[1])
            ptm_fh.close()
        else:
            raise Exception("Could not open {} file".format(ptm_file))


    def predict_modifiable_positions(self, seq):
        """Predict all modifiable positions in seq.
        Returns a dict position/AA.
        (this function is mainly for display / debug)
        """
        modifiable_pos = dict()
        for (i, aa) in enumerate(seq):
            if aa in self.authorised_ptms:
                if self.authorised_ptms[aa]['always'] :
                    modifiable_pos[i+1] = self.authorised_ptms[aa]['always'][:]
                if self.authorised_ptms[aa]['occasional']:
                    modifiable_pos[i+1] = self.authorised_ptms[aa]['occasional'][:]
        return modifiable_pos


    def predictPTM(self, seq):
        """Predict all possible combinations of PTMs occuring
        in seq, including all PTMs always occuring and some
        occasional PTMs and with a maximum of self.nb_PTMs_max PTMs.
        Return a list of dict.
        Each dict of the list is a combination of PTMs
        with key=pos (1-based) and value=modif name
        """

        # Create a list of dict of PTMs (key=pos, value=name)
        # This list contains all possible combinations of PTMs
        # with max 'self.nb_PTMs_max' PTMs.
        list_PTMs = list()

        # Make dicts with the position of modifiable AA as key
        # and the AA as the corresponding value.
        # We separate the always occuring PTMs and the occasional PTMs.
        dict_pos = dict()
        dict_pos['always'] = dict()
        dict_pos['occasional'] = dict()

        for (i, aa) in enumerate(seq):
            if aa in self.authorised_ptms:
                if self.authorised_ptms[aa]['always']:
                    dict_pos['always'][i+1] = self.authorised_ptms[aa]['always'][:]
                if self.authorised_ptms[aa]['occasional']:
                    dict_pos['occasional'][i+1] = self.authorised_ptms[aa]['occasional'][:]

        # Case with all always happening PTMs and no occasional PTM
        always_PTMs_list = list()
        always_PTMs_dict = dict()
        for pos in dict_pos['always']:
            for modif in dict_pos['always'][pos]:
                always_PTMs_dict[pos] = modif
        always_PTMs_list = [always_PTMs_dict]
        for elt in always_PTMs_list:
            list_PTMs.append(elt)

        # Complete with the occasional PTMs with the limit of
        # 'self.nb_PTMs_max' PTMs in the sequence.
        for i in range(1, self.nb_PTMs_max + 1):
            for elt in itertools.combinations(sorted(dict_pos['occasional'].keys()), i):
                previous_pos = always_PTMs_list.copy()
                for pos in elt:
                    current_pos = list()
                    for modif in dict_pos['occasional'][pos]:
                        for possibility in previous_pos:
                            current_modif = possibility.copy()
                            current_modif[pos] = modif
                            current_pos.append(current_modif)
                    previous_pos = current_pos[:]

                for combi in previous_pos:
                    list_PTMs.append(combi)

        return list_PTMs

