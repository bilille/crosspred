"""Class to perform in silico digestion of protein sequence
"""

import sys
import os
import pprint

from .peptide import *
from .utils import *


class Digestor():

    """Class to digest one or several fasta sequence(s)
    with a given peptidase.
    """

    def __init__(self, peptidases_file, misscleavage=0):
        """Initialisation of class
        peptidases_file: tabulated file of peptidases.
            The file must contain the following columns:
            'Name', 'Cleave', 'Not_cleave', 'N/C_term', 'Activated'.
            The column 'Activated' must contain 'yes' for the chosen
            peptidase and 'no' for all the others.
        misscleavage: nb of misscleavage(s) allowed
        """
        if not peptidases_file or not os.path.isfile(peptidases_file):
            raise Exception("You must provide a file with peptidases.\n")

        peptidases_fh = open(peptidases_file, 'r')

        header = peptidases_fh.readline()
        if header.strip() != "Name\tCleave\tNot_cleave\tN/C_term\tActivated":
            raise Exception("Peptidase file does not have the good format. Columns must be 'Name', 'Cleave', 'Not_cleave', 'N/C_term', 'Activated'")

        self.peptidase = None

        for line in iter(peptidases_fh.readline, ''):
            fields = line.rstrip().split("\t")
            if fields[4] == 'yes':
                if self.peptidase:
                    raise ValueError("You must choose only one peptidase.")
                self.peptidase = fields[0]
                self.aa_to_cut = fields[1].replace(' ', '').split(',')
                self.aa_to_not_cut = fields[2].replace(' ', '').split(',')
                self.side = fields[3]
        peptidases_fh.close()

        if not self.peptidase:
            raise ValueError("You must choose a peptidase.")

        self.misscleavage = misscleavage


    def digest(self, record):
        """Perform digestion of a sequence and
        return a dict of the digested peptides of the sequence
        with key=seq and value=all the possible positions
        (prot1:start1-end1,prot2:start2-end2,...).
        record is a SeqRecord object.
        """

        seq = str(record.seq)
        seq_id = str(record.id)
        peptides_dict = dict()

        # Mark the cleaving sites with '_'
        for aa in self.aa_to_cut:
            if self.side == 'C':
                seq = seq.replace(aa, aa + '_')
            else:
                seq = seq.replace(aa, '_' + aa)

        # Be careful that some AA can prevent a cleavage
        for aa in self.aa_to_not_cut:
            if self.side == 'C':
                seq = seq.replace('_' + aa, aa)
            else:
                seq = seq.replace(aa + '_', aa)

        start = 1
        end = 0

        # Prevent to have an empty peptid if the last AA in the peptid
        # is a cutable AA
        if seq[-1] == '_':
            seq = seq[:-1]

        # Cut the sequence on the cleavage sites
        seq_array = seq.strip().split('_')
        start_array = []
        end_array = []

        # Add peptides and peptides with misscleavages to the dict of peptides
        # One peptide can have several positions
        for i, pep in enumerate(seq_array):
            end += len(pep)

            if pep not in peptides_dict:
                peptides_dict[pep] = set()
            peptides_dict[pep].add("{}:{}-{}".format(seq_id, start, end))

            start_array.append(start)
            end_array.append(end)

            # deal with misscleavages
            if self.misscleavage > 0 and i > 0:
                current_pep = DigestedPeptide(pep, start, end)

                for j in range(i-1, max(-1, i - self.misscleavage - 1), -1):
                    previous_pep = DigestedPeptide(seq_array[j], start_array[j], end_array[j])
                    current_pep = concat_peptides(current_pep, previous_pep)
                    if current_pep.sequence not in peptides_dict:
                        peptides_dict[current_pep.sequence] = set()
                    peptides_dict[current_pep.sequence].add("{}:{}-{}".format(seq_id, current_pep.start, current_pep.end))

            # prepare for the next peptide
            start = end + 1

        return peptides_dict

    @classmethod
    def export_as_fasta(cls, peptide_obj_list, output_file):
        """This method takes as input a list of Peptide
        and writes the peptids in fasta format in output_file.
        Fasta entries headers contain the ID of the original protein
        and the positions of the peptid in that protein.
        Fasta entries are sorted by proteins ID and then start position.
        """
        with open(output_file, 'w') as fh:
            headers_dict = dict()
            for pep in peptide_obj_list:
                for pos in pep.position:
                    headers_dict[pos] = pep.sequence
            positions_detailed = [ [x.split(":")[0]] + x.split(":")[1].split('-') for x in headers_dict.keys()]
            positions_detailed.sort(key = lambda positions_detailed: (positions_detailed[0], int(positions_detailed[1]), int(positions_detailed[2]) ) )
            for pos in positions_detailed:
                new_pos = str(pos[0]) + ":" + str(pos[1]) + "-" + str(pos[2])
                fh.write(">" + new_pos + "\n")
                fh.write(headers_dict[new_pos] + "\n")

    @classmethod
    def export_as_tsv(cls, peptide_obj_list, output_file):
        """This method takes as input a list of Peptide
        and writes the peptids in TSV format in output_file.
        """
        with open(output_file, 'w') as fh:
            csv_header = ''
            csv_header += "Protein" + "\t"
            csv_header += "Peptide" + "\t"
            csv_header += "Start" + "\t"
            csv_header += "End" + "\t"
            csv_header += "Misscleavages" + "\n"
            fh.write(csv_header)
            headers_dict = dict()
            for pep in peptide_obj_list:
                for pos in pep.position:
                    headers_dict[pos] = pep.sequence
            positions_detailed = [ [x.split(":")[0]] + x.split(":")[1].split('-') for x in headers_dict.keys()]
            positions_detailed.sort(key = lambda positions_detailed: (positions_detailed[0], int(positions_detailed[1]), int(positions_detailed[2]) ) )
            for pos in positions_detailed:
                new_pos = str(pos[0]) + ":" + str(pos[1]) + "-" + str(pos[2])
                seq = headers_dict[new_pos]
                fh.write(str(pos[0]) + "\t")
                fh.write(seq + "\t")
                fh.write(str(pos[1]) + "\t")
                fh.write(str(pos[2]) + "\n")

