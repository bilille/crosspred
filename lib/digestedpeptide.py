"""Class to represent a peptide during the digestion
"""

class DigestedPeptide():
    """A DigestedPeptide is characterised by its sequence,
    and start and end.
    """

    def __init__(self, sequence='', start=0, end=0):
        """Initialise a DigestedPeptide.
        """
        if start == 0 and end == 0:
            start = 1
            end = len(sequence)
        if end < start:
            raise ValueError("Peptide end must be greater or equal to peptide start")
        if end - start + 1 != len(sequence):
            raise ValueError("Contradiction between the start & end positions and the sequence length")
        self.sequence = sequence
        self.start = start
        self.end = end

    def __str__(self):
        """This function is mainly used for dev/debug"""
        return "{}:{}-{}".format(self.sequence, self.start, self.end)
