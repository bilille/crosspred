"""Class to represent a peptide
"""

class Peptide():
    """A peptide is characterised by its sequence,
    its position(s) in the initial protein(s), its mass
    and its PTMs.
    """

    def __init__(self, sequence='', position='', mass=0, ptm=[]):
        """Initialise a peptide.
        ptm is a list of dict of possible PTMs with key=pos and value=modif name
        with maximum args.nbPTMmax PTMs. We store all possible combinations of PTMs.
        """
        self.sequence = sequence
        self.position = position
        self.mass = mass
        self.ptm = ptm

    def __str__(self):
        """This function is mainly used for dev/debug"""
        return "{};{};{};{}".format(self.sequence, self.position, self.mass, self.ptm)

