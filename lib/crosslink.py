"""Class to predict and describe crosslinks
between two peptidic sequences"""

import os
import sys

from .molmass import *
from .elements import *
from .peptide import *

class Predictor():

    """Class to predict all possible crosslinks
    between two peptides
    """

    def __init__(self, crosslink_file):
        """Initialisation of class
        crosslink_file: tabulated file of authorised crosslinks.
            The file must contain the following columns:
            'AA1', 'AA2', 'Molecule_lost', 'Activated'.
        The possible crosslinks will be stored in a dict with:
            Key: an AA that can be involved in a crosslink
            Value: a dict with
                key = the AA it will link to
                value = molecule(s) gained or lost during the crosslink
            For instance:
            {'D': {'K': ['-H2O']},
             'Y': {'Y': ['-H2']},
             'C': {'C': ['-H2', '-H2O']},
             'W': {'W': ['-H2']},
             'E': {'K': ['-H2O']},
             'K': {'E': ['-H2O'],
             'D': ['-H2O']}
            }
        """

        if not crosslink_file or not os.path.isfile(crosslink_file):
            raise Exception("You must provide a file with crosslinks.\n")

        crosslink_fh = open(crosslink_file, 'r')

        header = crosslink_fh.readline()
        if header.strip() != "AA1\tAA2\tMolecule_lost\tActivated":
            raise Exception("Crosslinks file does not have the good format. Columns must be 'AA1', 'AA2', 'Molecule_lost', 'Activated'.\nCurrent header is:<{}>".format(header.strip()))

        self.POSSIBLE_LINKS_FOR_AA = dict()
        for line in iter(crosslink_fh.readline, ''):
            fields = line.rstrip().split("\t")
            if fields[3] == 'yes':
                if fields[0] not in self.POSSIBLE_LINKS_FOR_AA:
                    self.POSSIBLE_LINKS_FOR_AA[fields[0]] = dict()
                if fields[1] not in self.POSSIBLE_LINKS_FOR_AA[fields[0]]:
                    self.POSSIBLE_LINKS_FOR_AA[fields[0]][fields[1]] = list()
                self.POSSIBLE_LINKS_FOR_AA[fields[0]][fields[1]].append(fields[2])
        crosslink_fh.close()

        self.crosslinks = []

    def predict(self, peptide1, peptide2, ptm_predictor):
        """Predict all possible crosslinks between 2 Peptide objects"""

        self.crosslinks = []
        ref_formula = Formula('peptide({})'.format(peptide1.sequence)) \
                    + Formula('peptide({})'.format(peptide2.sequence))

        for combination_PTM_pep1 in peptide1.ptm:
            for combination_PTM_pep2 in peptide2.ptm:

                PTM_mass_change_pep1 = 0
                pep1_PTM_names = ''
                pep1_PTM_pos = ''
                for pos in sorted(combination_PTM_pep1.keys()):
                    modif = combination_PTM_pep1[pos]
                    PTM_mass_change_pep1 += ptm_predictor.mass_ptms[modif]
                    pep1_PTM_names += modif + ';'
                    pep1_PTM_pos += str(pos) + ';'
                pep1_PTM_names = pep1_PTM_names[:-1]
                pep1_PTM_pos = pep1_PTM_pos[:-1]

                PTM_mass_change_pep2 = 0
                pep2_PTM_names = ''
                pep2_PTM_pos = ''
                for pos in sorted(combination_PTM_pep2.keys()):
                    modif = combination_PTM_pep2[pos]
                    PTM_mass_change_pep2 += ptm_predictor.mass_ptms[modif]
                    pep2_PTM_names += modif + ';'
                    pep2_PTM_pos += str(pos) + ';'
                pep2_PTM_names = pep2_PTM_names[:-1]
                pep2_PTM_pos = pep2_PTM_pos[:-1]

                # Make a dict with the linkable AAs from peptide1 in key
                # and their position in value.
                # Don't add AA with PTM.
                dict_aa_pep1 = dict()
                for (i, aa) in enumerate(peptide1.sequence):
                    if (i+1) in combination_PTM_pep1:
                        continue    # at this position the AA has a PTM
                    if aa in self.POSSIBLE_LINKS_FOR_AA:
                        try:
                            dict_aa_pep1[aa].append(i+1)
                        except:
                            dict_aa_pep1[aa] = list()
                            dict_aa_pep1[aa].append(i+1)

                # Same with peptide2
                dict_aa_pep2 = dict()
                for (i, aa) in enumerate(peptide2.sequence):
                    if (i+1) in combination_PTM_pep2:
                        continue    # at this position the AA has a PTM
                    if aa in self.POSSIBLE_LINKS_FOR_AA:
                        try:
                            dict_aa_pep2[aa].append(i+1)
                        except:
                            dict_aa_pep2[aa] = list()
                            dict_aa_pep2[aa].append(i+1)

                # Get the possible crosslinks between the 2 peptides
                for aa1 in dict_aa_pep1:
                    linkable_aa_for_aa1 = set(self.POSSIBLE_LINKS_FOR_AA[aa1]) & set(dict_aa_pep2)

                    if not linkable_aa_for_aa1:
                        # aa1 cannot make a link with any of the linkable aa in pep2
                        continue

                    for aa2 in linkable_aa_for_aa1:
                        for third_molecule in self.POSSIBLE_LINKS_FOR_AA[aa1][aa2]:
                            gain_or_loss = third_molecule[0]
                            third_molecule = third_molecule[1:]
                            f = ref_formula

                            if gain_or_loss == '-':
                                f -= Formula(third_molecule)
                            else:
                                f += Formula(third_molecule)

                            raw_formula = Formula(from_elements(f._elements))
                            mass = round(f.isotope.mass + PTM_mass_change_pep1 + PTM_mass_change_pep2, 4)

                            for pos1 in dict_aa_pep1[aa1]:
                                for pos2 in dict_aa_pep2[aa2]:
                                    c = Crosslink(aa1 + '-' + aa2,
                                                  pos1,
                                                  pep1_PTM_names,
                                                  pep1_PTM_pos,
                                                  pos2,
                                                  pep2_PTM_names,
                                                  pep2_PTM_pos,
                                                  raw_formula,
                                                  mass)
                                    self.crosslinks.append(c)




class Crosslink():

    """Class to represent a crosslink"""

    def __init__(self,
                 crosslinking='',
                 pos1=0,
                 ptm_names1='',
                 ptm_pos1='',
                 pos2=0,
                 ptm_names2='',
                 ptm_pos2='',
                 raw_formula='',
                 mass=0):
        self.crosslinking = crosslinking
        self.pos1 = pos1
        self.PTM_names1 = ptm_names1
        self.PTM_pos1 = ptm_pos1
        self.pos2 = pos2
        self.PTM_names2 = ptm_names2
        self.PTM_pos2 = ptm_pos2
        self.raw_formula = raw_formula
        self.mass = mass

    def __str__(self):
        output = str(self.crosslinking) + "\t" \
               + str(self.pos1) + "\t" \
               + str(self.PTM_names1) + "\t" \
               + str(self.PTM_pos1) + "\t" \
               + str(self.pos2) + "\t" \
               + str(self.PTM_names2) + "\t" \
               + str(self.PTM_pos2) + "\t" \
               + str(self.raw_formula) + "\t" \
               + str(self.mass)
        return output
