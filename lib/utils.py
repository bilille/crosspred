"""Useful functions that cannot be added to a class"""

import tempfile
import itertools

from .digestedpeptide import *
from .peptide import *
from .crosslink import *


electron_mass = 9.1093897 * 10**-31
proton_mass = Formula('H').isotope.mass - electron_mass
def multicharged_ion_mass(raw_mass, charge):
    return proton_mass + raw_mass / charge


def is_peptide_linkable(crosslinks_predictor, peptide):
    for aa in peptide:
        if aa in crosslinks_predictor.POSSIBLE_LINKS_FOR_AA:
            return True
    return False


#~ def chunks(l, n):
    #~ for i in range(0, len(l), n):
        #~ yield l[i:i+n]

def chunks(it, size_chunks):
    yield itertools.islice( it, 0, size_chunks )


def concat_peptides(peptide1, peptide2):
    """Function to concatenate 2 joining objects DigestedPeptide.
    The peptides must not be overlapping.
    There must not be any gap between the peptides.
    We can only concatenate them if
        peptide1.end == peptide2.start
        or peptide1.end == peptide2.start - 1
    """
    if peptide1.start > peptide2.start:
        peptide1, peptide2 = peptide2, peptide1

    if peptide1.start == peptide2.start or peptide1.end == peptide2.end:
        raise ValueError("Cannot concatenate peptides if one of them is included in the other")

    if peptide1.end > peptide2.start:
        raise ValueError("Cannot concatenate peptides if one of them is included in the other")

    if peptide2.start - peptide1.end > 1:
        raise ValueError("Cannot concatenate peptides if there is a gap between them (peptide 1: {} ; peptide 2: {}".format(peptide1, peptide2))

    if peptide1.end == peptide2.start:
        new_seq = peptide1.sequence[:-1] + peptide2.sequence
    else:
        new_seq = peptide1.sequence + peptide2.sequence

    new_start = peptide1.start
    new_end = peptide2.end

    return DigestedPeptide(new_seq, new_start, new_end)


def get_all_crosslinks_for_tuple_peptides(tuple_array, crosslinks_predictor, ptm_predictor, tmp):

    if tmp:
        if not os.path.exists(tmp) or not os.path.isdir(tmp):
            os.makedirs(tmp)
        f = tempfile.NamedTemporaryFile(delete=False, dir=tmp)
    else:
        f = tempfile.NamedTemporaryFile(delete=False)

    for elt in tuple_array:

        peptide1, peptide2 = elt

        # Predict all possibles crosslinks between the 2 peptides
        crosslinks_predictor.predict(peptide1, peptide2, ptm_predictor)

        # Print results
        for elt in crosslinks_predictor.crosslinks:
            content = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}-{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(peptide1.sequence,
                                elt.PTM_names1,
                                elt.PTM_pos1,
                                peptide2.sequence,
                                elt.PTM_names2,
                                elt.PTM_pos2,
                                elt.crosslinking,
                                elt.pos1,
                                elt.pos2,
                                elt.raw_formula,
                                elt.mass,
                                round(multicharged_ion_mass(elt.mass, 1), 4),
                                round(multicharged_ion_mass(elt.mass, 2), 4),
                                round(multicharged_ion_mass(elt.mass, 3), 4),
                                round(multicharged_ion_mass(elt.mass, 4), 4)
                        ).encode()
            f.write(content)

    f.close()
    return f.name

