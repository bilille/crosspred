#! /usr/bin/env python3


"""
Author: Isabelle GUIGON
Date: 2019-07-25
Developed on Python 3.5.2

This script takes as input a TSV file created by CROSSPRED pipeline.
It will read the theoric masses, sort them and store them in TSV files
named according to the mass.
This will facilitate the search of a theoric mass.
"""

import os
import sys
import argparse
import pprint
import math
import subprocess


def main(args):

    # Read the theorical masses
    theo_masses_dict = dict()
    with open(args.theo) as theo_fh:
        first_line = theo_fh.readline().strip()
        fields = first_line.split("\t")
        col_mass = 0
        col_n1 = 0
        col_n2 = 0
        col_n3 = 0
        col_n4 = 0

        for i, col in enumerate(fields):
            if col == "Mass":
                col_mass = i
            if col == "n=1":
                col_n1 = i
            if col == "n=2":
                col_n2 = i
            if col == "n=3":
                col_n3 = i
            if col == "n=4":
                col_n4 = i

        if len(fields) == col_n4 + 1:
            for line in theo_fh.readlines():
                line = line.strip()
                fields = line.split("\t")
                masses = fields[col_n1:col_n4+1]
                masses = [float(x) for x in masses]
                output_fields = fields[0:col_mass]
                for i, mass in enumerate(masses):
                    trunc_mass = math.trunc(mass)
                    mass_file_name = os.path.join(args.outdir, "theoric_mass_" + str(trunc_mass) + ".tsv")
                    with open(mass_file_name, "a") as f:
                        f.write("\t".join( [str(mass), str(i+1)] + output_fields ) + "\n")

    # sort the contents of all files
    list_files = os.scandir(args.outdir)
    for entry in list_files:
        if entry.is_file:
            infile_name = os.path.join(args.outdir, entry.name)
            process = subprocess.run('LC_NUMERIC="en_EN.UTF-8" sort -o {} {}'.format(infile_name, infile_name), shell = True)


if __name__ == '__main__':

    desc = "Script to process the theoric masses produced by CROSSPRED pipeline."

    command = argparse.ArgumentParser(prog = 'process_crosspred_output_masses.py',
        description = desc,
        usage = '%(prog)s [options]')

    command.add_argument('-t', '--theo', nargs = '?',
        type = str,
        required = True,
        help = 'An output tabulated file produced by CROSSPRED pipeline (required)')

    command.add_argument('-o', '--outdir', nargs = '?',
        type = str,
        default = os.path.dirname(os.path.realpath(__file__)),
        help = 'Output directory that will store the theoric masses')

    args = command.parse_args()

    if os.path.exists(args.outdir) and os.path.isdir(args.outdir):
        if os.listdir(args.outdir):   
            raise Exception("Output directory must be empty")
    else:
        os.makedirs(args.outdir)

    main(args)
